// This is the location in the folder structure you will put server side code
// Remember to update server response for REST-calls in server.js 

app.get('/', (req, res) => {
    res.send(`<a href="http://localhost:8080"> Hello world</a>`);
  
  })
  
  app.get('/getUsers', function (req, res) {
    
    db.query('SELECT * FROM users', function (err, result) {
      if (err) {
        res.status(400).send('Error in database operation.');
      } else {
        res.writeHead(200, { 'Content-Type': 'application/json' });
       // res.end(`<a href="http://localhost:8080">Tilbake til hjemmeside</a>`);
        res.end(JSON.stringify(result));
        console.log("Result: " + res);
  
      }
    });
  });
  
  
  
  app.post('/eksempel', async function (req, res) {
    res.send(req.body); 
    
  })
  
  app.post('/createUsers', async (req, res) =>{ 
    var email = req.body.email   
      if(email && req.body.password){
             
          //Bruker regular expressions bare for e-post når man registrer, ettersom at det ikke gir meninger å kreve et spesifikt format for kommentarer og titler, og det eneste kravet jeg har for passord er en lengde på minst 4
          
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //hentet fra https://emailregex.com/
          var OK = re.exec(email) 
          if(!OK){
            req.body.msg += ("error3")
            res.send(req.body) 
            res.end()
          }else{ 
            if(req.body.password.length < 4){
              req.body.msg += ("error4")
              res.send(req.body) 
              res.end()
            }else{
              db.query('SELECT * FROM users WHERE email = ?', [email], function(error, results, fields) { 
  
                if (results.length > 0) { 
                  req.body.msg += ("error2")
                  res.send(req.body)
                } else {
                  var hashedPassword = hashCode(req.body.password)
                  let user = {email:email, password:hashedPassword,userType:"user",bildetype:req.body.bildeType}
                  let sql = 'INSERT INTO users SET ?'; 
  
                  let query = db.query(sql, user, (err, result) => { 
                    //Får tak i den nyskapte IDen  
                    console.log(JSON.stringify(result["insertId"])) 
  
                      req.body.userid = JSON.stringify(result["insertId"]) 
                      req.body.msg += ("Du er innlogget som " + req.body.email)
                      res.send(req.body)    
  
                  }); 
               
                }
              });     
           
          }
        }
      }else{
        req.body.msg += ("error") 
        res.send(req.body)  
        res.end()
      }  
  }); 
  
  app.post('/deleteUser', async (req,res)=>{
    let sql = `DELETE FROM users WHERE uid = 4`;
  
    let query = db.query(sql, (err, result) => {
      if(err) throw err;
      console.log(result);
      res.send('Post deleted')
    });
    if(err) throw err;
  })
  
  app.post('/logIn', (request,response)=>{ 
    
    var email = request.body.email;
   
    var hashedPassword = hashCode(request.body.password)
    var usertype = request.body.usertype
      if (email && hashedPassword) {  
      
          db.query('SELECT * FROM users WHERE email = ? AND password = ?', [email, hashedPassword], function(error, results, fields) {
              if (results.length > 0) { 
        
          request.body.usertype = (results[0])['userType'] 
          request.body.bildeType = (results[0])['bildetype'] 
          request.body.userid = (results[0])['uid']
          response.send(request.body)
              } else {
          request.body.msg = ("error2")
                  response.send(request.body);
              }
              response.end();
          });
      } else {
      request.body.msg = ("error")
          response.send(request.body);
    }  
  
    
  });
  
  app.post('/oppgrader', async (req,res)=>{
    let newUserType = 'moderator'
    let sql = `UPDATE users SET userType= '${newUserType}' WHERE email = '${req.body.email}'`;
  
    let query = db.query(sql, (err, result) => {
      if(err) throw err;
      req.body.msg = "Bruker oppdatert" 
  
    });
    
    
    
  })  
  
  app.post('/ingenOppgradering', async (req,res)=>{
    let sql = `DELETE FROM forespoersler WHERE email = '${req.body.email}'`;
  
    let query = db.query(sql, (err, result) => {
      if(err) throw err; 
      req.body.msg = "Brukeren ble ikke oppgradert" 
      res.send(req.body);
    });
    if(err) throw err;
  }) 
  
  
  app.post('/forespoersel', async (req,res)=>{
    let forespoersel = {email:req.body.email, text:req.body.text}
        let sql = 'INSERT INTO forespoersler SET ?'; 
        
         let query = db.query(sql, forespoersel, (err, result) => {
          req.body.msg = ("La til " + req.body.email + req.body.text[0])
          res.send(req.body)
         });
         if(err) throw err;  
  })   
  
  app.get('/forespoersler', function (req, res) {
   
    var antallForespoersler = 0; 
    var resultater; 
    db.query('SELECT * FROM forespoersler', function (err, result) {
      if (err) {
        res.status(400).send('Error in database operation.');
      } else {
        
  
  
        //Antall forespoersler  
        antallForespoersler = result.length  
        resultater = result
        //res.end("   " + JSON.stringify(result)); 
        
  
      }   
      var idListe = [] 
      
      for(var i = 0; i < antallForespoersler; i++){
        idListe.push((resultater[i])['id'])
      }
      res.render('index.ejs',{forespoerselInfo: antallForespoersler, forespoerselIder: idListe})
    }); 
    
  });
  
  app.get('/forespoersler/:id', function (req, res) {
    var id = req.params.id
    db.query('SELECT * FROM forespoersler WHERE id = ?', [id], function(error, results, fields){
      if (error) {
        results.status(400).send('Error in database operation.');
      } else {
        res.render('forespoersel.ejs',{forespoerselNavn: (results[0])['email'],forespoerselText: (results[0])['text'] })
  
      } 
      
      
    }); 
  }) 
  app.get('/forespoersler/oppgrader/:navn', function (req, res) {
    var navn = req.params.navn
    let newUserType = 'moderator'
    let sql = `UPDATE users SET userType= '${newUserType}' WHERE email = '${navn}'`;
  
    db.query(sql, (err, result) => {
      if(err) throw err; 
    }); 
  
    db.query(`DELETE FROM forespoersler WHERE email = '${navn}'`, (err, result) => {
      if(err) throw err;
    }); 
  
    res.render('oppgrader.ejs')  
  }) 
  app.get('/forespoersler/ikkeOppgrader/:navn', function (req, res) {
    var navn = req.params.navn
    let sql = `DELETE FROM forespoersler WHERE email = '${navn}'`;
  
    let query = db.query(sql, (err, result) => {
      if(err) throw err; 
    });
    res.render('ikkeOppgrader.ejs')  
  })
  
  function hashCode(s){
    return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
  } 
  
  app.get('/posts/:navn', function (req, res) { 
      var navn = req.params.navn; 
      var id = "id"; 
      var brukerType = "user" 
      var postDataListe = [] 
      var userDataListe = []  
      //Går gjennom alle brukere og legger dem til i en liste som gjør det lettere å finne eposten til en userId som står i post table 
      db.query('SELECT * FROM users', function(error, usersResults, fields) { 
        for(var i = 0; i < usersResults.length; i++){
          var user = { 
            uid: (usersResults[i])['uid'],
            epost: (usersResults[i])['email'] 
            
            
          } 
          userDataListe.push(user)
  
        }
        //Finner bruker med emailen til den som er logget inn i sessionen slik at vi kan passere informasjon som id og brukertype videre fordi det bestemmer innhold på siden som hvilke funksjoner man har tilgang til 
        db.query('SELECT * FROM users WHERE email = ?', [navn], function(error, userResults, fields) { 
         
          db.query('SELECT * FROM posts', function(err, postResult) {
         
         
            id = (userResults[0])['uid']  
            brukerType = (userResults[0])['userType'] 
    
            for(var i = 0; i < postResult.length; i++){ 
    
              var brukerEpost = ""    
              brukerEpost = userDataListe.find(item => item.uid === (postResult[i])['user']);
              var postId =  (postResult[i])['pid']
              var posterId =  (postResult[i])['user']
              var tittel =  (postResult[i])['title'] 
              var tekst =  (postResult[i])['content']
              var score =  (postResult[i])['score']
              var post = {
                postId:  postId  , 
                posterId, posterId,
                tittel:  tittel ,
                tekst:   tekst , 
                score:   score ,
                brukerNavn: JSON.stringify(brukerEpost['epost'])
              }
              postDataListe.push(post)
              
              
            } 
            var postDataListeTid = [postDataListe]
            res.render('posts.ejs',{brukerId: id,brukerType: brukerType,postInformasjonSortertTid: postDataListeTid,brukerNavn: navn,postInformasjon: postDataListe})  
         
    
          }); 
         
        }); 
        
  
  
      })
      
      
    
  });  
  
  app.get('/posts/:brukerId/kommentarer/:brukerType/:posterId/:postId', function (req, res) {
    var brukerId = req.params.brukerId;  
    var posterId = req.params.posterId
    var postId = req.params.postId;  
    var brukerType = req.params.brukerType
    var kommentarListe = []  
    var userDataListe = [] 
    //Finner alle uider og eposter fra users for å kunne finne en brukers epost gjennom deres id fra kommentarer tablet
    db.query('SELECT * FROM users', function(error, usersResults, fields) { 
      for(var i = 0; i < usersResults.length; i++){
        var user = { 
          uid: (usersResults[i])['uid'],
          epost: (usersResults[i])['email'] 
          
          
        } 
        userDataListe.push(user)
  
      }
      db.query('SELECT * FROM comments WHERE post = ?', [postId], function(error, cResults, fields) {
        console.log(cResults)
        for(var i = 0; i < cResults.length; i++){ 
      
          var brukerNavn = userDataListe.find(item => item.uid === (cResults[i])['user']);
          
          var kommentar = { 
           
            tekst:   (cResults[i])['comment'] , 
            score : (cResults[i])['score'] ,  
            id : (cResults[i])['cid'] ,  
            userId : (cResults[i])['user'] ,  
            brukerNavn: JSON.stringify(brukerNavn['epost'])
          }  
          kommentarListe.push(kommentar)
        } 
      res.render('kommentarer.ejs',{brukerType: brukerType, brukerId: brukerId, postId: postId, posterId: posterId,kommentarInformasjon: kommentarListe})  
     });   
    });
    
    
    //kommentar data skal bestå av navn og innhold
  
  }); 
  
  /*app.post('/posts', async (req,res)=>{
    sessionStorage.setItem("name",)
  })*/   
  
  app.post('/skrivPost/:id', async (req,res)=>{
    var id = req.params.id;
    let user = {user:id, title:req.body.title,content:req.body.text}
        let sql = 'INSERT INTO posts SET ?';
         let query = db.query(sql, user, (err, result) => {
          
         }); 
         res.render('skrevPost.ejs',{brukerId: id})
  })      
  
  
  
  app.get('/skrivPosts/:id', function (req,res){
    var id = req.params.id;
    
        
         res.render('skrivPosts.ejs',{brukerId: id})
  })    
  
  app.post('/skrivKommentar/:postId/:userId', async (req,res)=>{
    var postId = req.params.postId; 
    var userId = req.params.userId;
    let comment = {user:userId, post:postId,comment:req.body.text}
        let sql = 'INSERT INTO comments SET ?';
         let query = db.query(sql, comment, (err, result) => {
          
         }); 
         res.render('utskrift.ejs',{utskrift: "Skrev kommentar"}) 
  })      
  
  
  app.post('/search/:navn', async (req,res)=>{
    var search = req.body.search  
    var brukerType;
    var id ; 
    var navn = req.params.navn; 
    var userDataListe = []; 
    var postDataListe = [];
    db.query('SELECT * FROM users', function(error, usersResults, fields) { 
      for(var i = 0; i < usersResults.length; i++){
        var user = { 
          uid: (usersResults[i])['uid'],
          epost: (usersResults[i])['email'] 
        } 
        userDataListe.push(user) 
       } 
       db.query('SELECT * FROM users WHERE email = ?', [navn], function(error, userResult, fields) { 
        console.log(navn);
        id = (userResult[0])['uid']  
        brukerType = (userResult[0])['userType'] 
        
         db.query('SELECT * FROM posts WHERE title LIKE ?','%' + search + '%', function(error, postResult, fields)  { 
           if(postResult.length == 0){
            res.render('ingenFunn.ejs') 
            res.end();
           }
          for(var i = 0; i < postResult.length; i++){ 
  
              var brukerEpost = ""    
              brukerEpost = userDataListe.find(item => item.uid === (postResult[i])['user']);
              var postId =  (postResult[i])['pid']
              var posterId =  (postResult[i])['user']
              var tittel =  (postResult[i])['title'] 
              var tekst =  (postResult[i])['content']
              var score =  (postResult[i])['score'] 
  
              var post = {
                postId:  postId  , 
                posterId, posterId,
                tittel:  tittel ,
                tekst:   tekst , 
                score: score ,
                brukerNavn: JSON.stringify(brukerEpost['epost'])
              }
              postDataListe.push(post)
      
              
            } 
            res.render('soekresultater.ejs',{postInformasjon: postDataListe,brukerType:  brukerType, brukerNavn: navn, brukerId: id})  
         }); 
         
        })
        });
  })     
  app.post('/upvotePost/:id', async (req,res)=>{
    var id = req.params.id;   
    var score; 
      db.query('SELECT * FROM posts WHERE pid = ?',[id], function(error, postResult, fields) {
        score = (postResult[0])['score'] 
        score ++;
         db.query('UPDATE posts SET score = ? WHERE pid = ?',[score,id], (err, result) => {
          console.log(score);
          res.render('utskrift.ejs',{utskrift: "Post upvoted"}) 
         }); 
         
        
        })
  })   
  app.post('/downvotePost/:id', async (req,res)=>{
    var id = req.params.id;   
    var score;  
      db.query('SELECT * FROM posts WHERE pid = ?',[id], function(error, postResult, fields) {
        score = (postResult[0])['score']
         db.query('UPDATE posts SET score = ? WHERE pid = ?',[score-1,id], (err, result) => {
          
          res.render('utskrift.ejs',{utskrift: "Post downvoted"}) 
         }); 
          
        
        })
  })          
  app.post('/upvoteKommentar/:id', async (req,res)=>{
    var id = req.params.id;   
    var score; 
      db.query('SELECT * FROM comments WHERE cid = ?',[id], function(error, cResult, fields) {
        score = (cResult[0])['score'] 
        score ++;
         db.query('UPDATE comments SET score = ? WHERE cid = ?',[score,id], (err, result) => {
          res.render('utskrift.ejs',{utskrift: "Kommentar upvoted"}) 
         }); 
         
        
        })
  })   
  app.post('/downvoteKommentar/:id', async (req,res)=>{
    var id = req.params.id;   
    var score;  
      db.query('SELECT * FROM comments WHERE cid = ?',[id], function(error, cResult, fields) {
        score = (cResult[0])['score']
         db.query('UPDATE comments SET score = ? WHERE cid = ?',[score-1,id], (err, result) => {
          
          res.render('utskrift.ejs',{utskrift: "Kommentar downvoted"}) 
         }); 
          
        
        })
  })      
  app.post('/redigerPost/:id', async (req,res)=>{
    var id = req.params.id;  
    var title = req.body.title;   
    var text = req.body.text;
    
         db.query('UPDATE posts SET title = ? AND content = ? WHERE pid = ?',[title,text,id], (err, result) => {
          
          res.render('utskrift.ejs',{utskrift: "Post redigert"}) 
        }); 
          
        
  
  })      
  app.post('/slettPost/:id', async (req,res)=>{
    var id = req.params.id;
    //Sjekker om det er noen kommentarer under posten
    db.query('SELECT * FROM comments WHERE post = ?',[id], function(error, commentResults, fields) {
      //Hvis det er det, slett kommentarene
      if(commentResults.length != 0){
        db.query(`DELETE FROM comments WHERE post = ?`,[id], (err, result) => {
          db.query(`DELETE FROM posts WHERE pid = ?`,[id], (err, result) => {
            res.render('utskrift.ejs',{utskrift: "Post slettet"}) 
          }); 
        });  
        //Hvis ikke, bare slett post
      }else{
        db.query(`DELETE FROM posts WHERE pid = ?`,[id], (err, result) => {
          res.render('utskrift.ejs',{utskrift: "Post slettet"}) 
        }); 
      }
      
    });
  })      
  app.post('/blokkPost/:title/:text/:posterName/:postId/:userId', async (req,res)=>{
    var pid = req.params.postId;
    var uid = req.params.userId; 
    var poster = req.params.posterName; 
    var title = req.params.title;
    var text = req.params.text;
    let blockedPost = {title: title,text: text,user: poster,blockedBy:uid}
    
    db.query(`INSERT INTO blockedPost SET ?`,blockedPost, (err, results) => {
      //Sjekk om det er det
      db.query('SELECT * FROM comments WHERE post = ?',[pid], function(error, commentResults, fields) {
        //Hvis det er det, slett kommentarene
        if(commentResults.length != 0){
          db.query(`DELETE FROM comments WHERE post = ?`,[pid], (err, result) => {
            db.query(`DELETE FROM posts WHERE pid = ?`,[pid], (err, result) => {
              res.render('utskrift.ejs',{utskrift: "Post blokkert"}) 
            });  
          });  
          //Hvis ikke, bare slett fra post
        }else{
          db.query(`DELETE FROM posts WHERE pid = ?`,[pid], (err, result) => {
            res.render('utskrift.ejs',{utskrift: "Post blokkert"}) 
          });  
        }
        
      });
    
        
      });  
   
  
  })   
  app.post('/redigerKommentar/:id', async (req,res)=>{
    var id = req.params.id;   
    var text = req.body.text;
    
         db.query('UPDATE comments SET comment = ? WHERE cid = ?',[text,id], (err, result) => {
          
          res.render('utskrift.ejs',{utskrift: "Kommentar redigert"}) 
        }); 
          
        
  
  })      
  app.post('/slettKommentar/:id', async (req,res)=>{
    var id = req.params.id;
  
  
        db.query(`DELETE FROM comments WHERE cid = ?`,[id], (err, result) => {
          res.render('utskrift.ejs',{utskrift: "Kommentar slettet"}) 
        });  
   
   
  
  })     
  
  app.post('/blokkKommentar/:text/:posterName/:cid/:userId', async (req,res)=>{
    var id = req.params.userId; 
    var cid = req.params.cid;
    var poster = req.params.posterName; 
    var text = req.params.text;
    let blockedComment = {text: text,user: poster,blockedBy:id}
    
    db.query(`INSERT INTO blockedComment SET ?`,blockedComment, (err, result) => {
        db.query(`DELETE FROM comments WHERE cid = ?`,[cid], (err, result) => {
          res.render('utskrift.ejs',{utskrift: "Kommentar blokkert"}) 
        });  
      });  
   
  
  })    
  
  app.get('/informasjonsSide/:id', async (req,res)=>{
    var id = req.params.id;
  
  
    db.query('SELECT * FROM users WHERE uid = ?',[id], function(error, usersResults, fields) { 
  
     
      
        
        res.render('informasjonsSide.ejs',{email: (usersResults[0])['email'], userType: (usersResults[0])['userType'],bildeType: (usersResults[0])['bildetype'], id: (usersResults[0])['uid']  }) 
        
  
    })  
  })       
  app.post('/endreEpost/:id', async (req,res)=>{
    var id = req.params.id;
    var email = req.body.email   
    if(email){
           
  
        
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //hentet fra https://emailregex.com/
        var OK = re.exec(email) 
        if(!OK){
          res.render('utskrift.ejs',{utskrift: "Eposten er ikke gyldig"}) 
        }else{ 
      
            db.query('SELECT * FROM users WHERE email = ?', [email], function(error, results, fields) { 
  
              if (results.length > 0) { 
                res.render('utskrift.ejs',{utskrift: "Noen andre bruker den eposten"}) 
              } else {
                  db.query('UPDATE users SET email = ? WHERE uid = ?', [email,id], function(error, results, fields) { 
  
                            
                      res.render('utskrift.ejs',{utskrift: "Epost endret. Vennligst logg ut og inn"}) 
        
                  });      
              }
          })
         
        
       }
    }else{
      res.render('utskrift.ejs',{utskrift: "Skriv inn en epost"}) 
    }  
    
  
  })   
  app.post('/endreBilde/:bildeType/:id', async (req,res)=>{
    var id = req.params.id;
    var bildeType = req.params.bildeType;
     
        
            db.query('UPDATE users SET bildetype = ? WHERE uid = ?', [bildeType,id], function(error, results, fields) { 
  
                
              
              res.render('utskrift.ejs',{utskrift: "Bildetype endret, men det kan hende du må vente litt før du ser endringen"}) 
         
            });     
         
  
    
    
  
  })  
  app.get('/listOfModeratorsAndUsers', async (req,res)=>{
    
    var userDataListe = []
  
    db.query('SELECT * FROM users WHERE userType != ?',["admin"] , function(error, usersResults, fields) { 
      for(var i = 0; i < usersResults.length; i++){
        var user = { 
          uid: (usersResults[i])['uid'],
          epost: (usersResults[i])['email'],   
          userType: (usersResults[i])['userType'],  
          bildeType: (usersResults[i])['bildetype'] 
        }  
        
        userDataListe.push(user) 
       } 
      
       res.render('moderatorsAndUsers.ejs',{userDataListe: userDataListe}) 
      })
  
  })    
  
  app.get('/usersListe', async (req,res)=>{
    
    var userDataListe = []
  
    db.query('SELECT * FROM users WHERE userType = ?',["user"] , function(error, usersResults, fields) { 
      for(var i = 0; i < usersResults.length; i++){
        var user = { 
          uid: (usersResults[i])['uid'],
          epost: (usersResults[i])['email'],   
          userType: (usersResults[i])['userType'],  
          bildeType: (usersResults[i])['bildetype'] 
        }  
        
        userDataListe.push(user) 
       } 
      
       res.render('moderatorsAndUsers.ejs',{userDataListe: userDataListe}) 
      })
  
  })    
  
  app.get('/blockedListe/:id', async (req,res)=>{
    var id = req.params.id;
    var commentDataListe = [] 
    var postDataListe = []
  
    db.query('SELECT * FROM blockedComment WHERE blockedBy = ?',[id] , function(error, commentResults, fields) { 
      
      for(var i = 0; i < commentResults.length; i++){
        var comment = { 
          text: (commentResults[i])['text'],
          user: (commentResults[i])['user'],   
        }  
        
        commentDataListe.push(comment) 
       }  
       db.query('SELECT * FROM blockedPost WHERE blockedBy = ?',[id] , function(error, postResults, fields) { 
      
        for(var i = 0; i < postResults.length; i++){
          var post = { 
            title: (postResults[i])['title'],
            text: (postResults[i])['text'],    
            user: (postResults[i])['user'],  
          }  
          
          postDataListe.push(post) 
         } 
        
         res.render('blockedListe.ejs',{commentDataListe: commentDataListe, postDataListe: postDataListe }) 
        })
      
       
      })
  
  })   
   
  