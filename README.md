# Prosjekt - PROG2053

## Installation notes: 
- Install nodeJs
- Run "npm i" in both the server and client folders of the project.    
- While in the root folder of the project: "docker-compose up -d" - this will take a long time the first time you do it.
- Are you getting an error after doing "docker-compose up -d", saying "[...] Filesharing has been cancelled"? 
-> Go to Docker for Windows app (or similar) -> Settings -> Resources -> File sharing -> Add all your drives (or play around with figuring out what exactly you need).
- Are you getting an error saying "npm ERR! ch() never called"? 
-> Delete "package-lock.json" from the client directory, then build the client again using "docker-compose build client"

Want to reset your containers and volumes fully? 
- "docker system prune -a --volumes"

Want to get in to a container for some reason? 
- "docker-compose exec <containername> bash" 

## Group members:     
* Abdisalan M. Hussein (494334)
* Endre Heksum (526349)
* Erlend Thingnes (526370)
* Henrik M. Karlsen (526346)
* Mahamed Said (494343)
   
## Setup: 
- docker-compose up -d   
The project runs on localhost:8080   
# How to Use:
* First time visitors have to register themselves (you have to have an account to see posts or even write a post of your own)
* All users have non-privileged users in the beginning, to change privilege to moderator you need to send request to Admin(s):
    * write a quick message on the field and click "Spoer om oppgradering til moderator"
    * fire up a new tab and login with admin user and accept/decline the request
    * If accepted the user will have moderator next time they sign in
* There is sadly no logout function, the user have to close that particular tab and fire up a new one and login again!


