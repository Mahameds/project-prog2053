import { LitElement, html, css } from 'lit-element';

export class TestELement2 extends LitElement {
  static get properties(){
  return{
      userId: {
        type: String
      },
      msg: {
        type: String
      },
      myBool: {
        type: Boolean
      }, 
      oppgradertMelding: {
        type: String 
      }

    }
  }
  constructor(){
    super();
    this.userId = "";
    this.msg = "";  
    this.oppgradertMelding = ""
    //this.brukere = "";
  }

    static styles = css`
    :host {
        display: block;
    }
    `;
    render() {

        return html`
      
      
        
        ${sessionStorage.getItem("name") ? 
        html`<p> Du er innlogget som ${sessionStorage.getItem("name")}</p>
        <a href="${window.MyAppGlobals.serverURL}informasjonsSide/${sessionStorage.getItem("userId")}">Din informasjons side</a> 
        <a href="${window.MyAppGlobals.serverURL}posts/${sessionStorage.getItem("name")}">Se posts(Eller skriv)</a>
        `: 
        html``
        }
        

         
        ${sessionStorage.getItem("usertype") == "user" ? 
        html`<p>Innlogget som vanlig bruker</p>
        <p>Her kan du spoerre om aa bli en moderator
        <form> 

        <input type="text" name ="text">
        <button @click="${this.forespoersel}" type="button" value="register">Spoer om oppgradering til moderator</button>
        </form>
        
        
        
        `: 
        html``
        }   
        ${sessionStorage.getItem("usertype") == "moderator" ? 
        html`<p>Innlogget som moderator</p>
        <li><a href="${window.MyAppGlobals.serverURL}usersListe">Liste av brukere </a></li>
        <li><a href="${window.MyAppGlobals.serverURL}blockedListe/${sessionStorage.getItem("userId")}">Se liste over hva du har blokkert</a></li>
     
        `: 
        html`
        
        `
        }    
        ${sessionStorage.getItem("usertype") == "admin" ? 
        html`<p>Innlogget som admin</p> 
        <li><a href="${window.MyAppGlobals.serverURL}listOfModeratorsAndUsers">Liste av moderatorer og brukere </a></li>
        <li><a href="${window.MyAppGlobals.serverURL}blockedListe/${sessionStorage.getItem("userId")}">Se liste over hvem du har blokkert </a></li>
        <li><a href="${window.MyAppGlobals.serverURL}forespoersler">Se liste over folk som vil bli moderatorer </a></li>
     
        `: 
        html`
        
        `
        }    
        `
    }
    forespoersel(e){
      const data = new FormData(e.target.form);
      var formBody = [];
      for (var pair of data.entries()) {
        console.log(pair[0]+ ', ' + pair[1])
        var encodedKey = encodeURIComponent(pair[0]);
        var encodedValue = encodeURIComponent(pair[1]);
        formBody.push(encodedKey + "=" + encodedValue);
      }


      var encodedKey = encodeURIComponent("msg");
      var encodedValue = encodeURIComponent("ingen msg");
      formBody.push(encodedKey + "=" + encodedValue);    

      var encodedKey = encodeURIComponent("email");
      var encodedValue = encodeURIComponent(sessionStorage.getItem("name"));
      formBody.push(encodedKey + "=" + encodedValue);  

      formBody = formBody.join("&");
      fetch(`${window.MyAppGlobals.serverURL}forespoersel`, { 
          method: 'POST',

          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: formBody
      })
      .then(response => response.json()
    ).then(beskjed => {
          this.oppgradertMelding = beskjed['msg']; 
          
      })
    }
   
    eksempel(){ 
      

        
        html`${this.eksempelFunksjon()}`  
        
      
    }  
    
    eksempelFunksjon(){
        var formBody = [];
        
        //Melding som kan bli printet ut 
        var encodedKey = encodeURIComponent("msg");
        var encodedValue = encodeURIComponent("");
        formBody.push(encodedKey + "=" + encodedValue); 
        formBody = formBody.join("&");
        fetch(`${window.MyAppGlobals.serverURL}eksempel`, { 

            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        })
        .then(response => response.json()
      ).then(beskjed => {
            this.msg = beskjed['msg']; 

        })
    }
    

}
customElements.define('hoved-innhold', TestELement2);
