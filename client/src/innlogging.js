import { LitElement, html, css } from 'lit-element';

export class TestELement extends LitElement {
  static get properties(){
  return{
      userId: {
        type: String
      },
      msg: {
        type: String
      }, 
      bildeType: {
        type: String
      },
      myBool: {
        type: Boolean
      }
    }
  }
  constructor(){
    super();
    this.userId = "";
    this.msg = "";  

  }

  static get styles(){
    return css`
      
      img{
        height: 250px; 
        width: 150px;
      }
    `
    }
    
    render() {
     
        return html`  
        ${this.msg ? 
       html`<p>${this.msg}</p>` : 
       html``}
       ${sessionStorage.getItem("name") ? 
       html`<p>${sessionStorage.getItem("name")}</p>` :
       html`
       <h1>Welcome To Our Final Project</h1>
       <div class="split left">
       <nav>
       
       <h3> Please register</h3>
       <form>
       <input type="text" name="email" placeholder ="email" required> <br>
        <input type="password" name="password" placeholder = "password" required> <br> 
       <button @click="${this.register}" type="button" value="register">Register</button> <br>
       <button @click="${(e)=>sessionStorage.setItem("bildeType","Gorilla")}" type="button" value="setPicture" >ProfilePicture</button> <br>
  
      
       </form> 
        </nav>
        </div>
        <div class="split right">
        <aside>
        <h3>Log in if you already have an account</h3> 
        <form>
        <input type="text" name="email" placeholder ="email" required> <br>
        <input type="password" name="password" placeholder = "password" required> <br>
        <button @click="${this.logIn}" type="button" value="logIn">Logg inn</button> <br>
        </aside>
        </form>
        </div>
        
          `}
          <!--Let the user choose image by clicking on it-->
          ${sessionStorage.getItem("bildeType") ?
          html``    
          :html`<p>Profilbilde: </p>
          <img src="src/intetBilde.jpg" alt="Graatt bilde">
            `}  
          ${sessionStorage.getItem("bildeType") == "Gorilla"?
            html`<p>Bilde: </p>
          
            <img src="src/gorilla.jpg" alt="Gorilla">
            `  :
            html``} 
          ${sessionStorage.getItem("bildeType") == "intetBilde"?
            html`<p>Bilde: </p>
            <img src="src/intetBilde.jpg" alt="Graatt bilde">
            `  :
            html``} 

        <!--<a href="${window.MyAppGlobals.serverURL}getUsers">Database med flere brukere</a>-->

        `;
        
    }

    register(e) {
        const data = new FormData(e.target.form);
        var formBody = [];
        for (var pair of data.entries()) {
          console.log(pair[0]+ ', ' + pair[1])

          var encodedKey = encodeURIComponent(pair[0]);
          var encodedValue = encodeURIComponent(pair[1]);
          formBody.push(encodedKey + "=" + encodedValue);

        } 
        //Melding som kan bli printet ut 
        var encodedKey = encodeURIComponent("msg");
        var encodedValue = encodeURIComponent("placeholder");
        formBody.push(encodedKey + "=" + encodedValue); 
        
        //Et par verdier vi vil hente fra server og så lagre i session, usertpe og userid
        var encodedKey = encodeURIComponent("usertype");
        var encodedValue = encodeURIComponent("placeholder");
        formBody.push(encodedKey + "=" + encodedValue); 
        
        var encodedKey = encodeURIComponent("userid");
        var encodedValue = encodeURIComponent("placeholder");
        formBody.push(encodedKey + "=" + encodedValue); 

        if (sessionStorage.getItem("bildeType") === null) {
          sessionStorage.setItem("bildeType","intetBilde")
        } 
        var encodedKey = encodeURIComponent("bildeType");  
        var encodedValue = encodeURIComponent(sessionStorage.getItem("bildeType"));
        formBody.push(encodedKey + "=" + encodedValue); 

        formBody = formBody.join("&");
        fetch(`${window.MyAppGlobals.serverURL}createUsers`, { // Bytt dette med din path
            method: 'POST',
            //credentials: "include",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        })
        .then(response => response.json()
      ).then(beskjed => {
            this.msg = beskjed['msg']; 
            if(this.msg != "error" && this.msg !="error2" && this.msg != "error3" && this.msg !="error4"){
              this.myBool = true; 
              sessionStorage.setItem("name",beskjed['email']); 
              sessionStorage.setItem("usertype","user");     
              sessionStorage.setItem("userId",beskjed['userid']);    
              location.reload(); 
              return false;
            } else if(this.msg == "error2"){
              this.msg = "Det finnes allerede en bruker med den eposten"
            }else if(this.msg == "error3"){
              this.msg = "Ikke gyldig epost"
            }else if(this.msg == "error4"){
              this.msg = "Skriv inn et passord paa minst 4 bokstaver"
            }else{
              this.msg = "Vennligst fyll begge feltene"
            }
        })
        
          

    } 
    eksempel(){ 
      
    
      
      html`${this.eksempelFunksjon()}`  
      
    
    } 
    eksempelFunksjon(){ 
      
  
      if(sessionStorage.getItem("bildeType") == ""){
        sessionStorage.setItem("bildeType","undefined");
      
      }
    
  }    
    logIn(e) { 
      
        const data = new FormData(e.target.form);
        var formBody = [];
        for (var pair of data.entries()) {
          console.log(pair[0]+ ', ' + pair[1])
          var encodedKey = encodeURIComponent(pair[0]);
          var encodedValue = encodeURIComponent(pair[1]);
          formBody.push(encodedKey + "=" + encodedValue);
        }


        var encodedKey = encodeURIComponent("usertype");
        var encodedValue = encodeURIComponent("ingen bruker");
        formBody.push(encodedKey + "=" + encodedValue);

        var encodedKey = encodeURIComponent("msg");
        var encodedValue = encodeURIComponent("ingen msg");
        formBody.push(encodedKey + "=" + encodedValue); 

        var encodedKey = encodeURIComponent("bildeType");
        var encodedValue = encodeURIComponent("");
        formBody.push(encodedKey + "=" + encodedValue); 

        var encodedKey = encodeURIComponent("userid");
        var encodedValue = encodeURIComponent("");
        formBody.push(encodedKey + "=" + encodedValue); 
        formBody = formBody.join("&");
        fetch(`${window.MyAppGlobals.serverURL}logIn`, { // Bytt dette med din path
            method: 'POST',
            //credentials: "include",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        })
        .then(response => response.json()
      ).then(beskjed => {
            this.msg = beskjed['msg']; 
            if(this.msg != "error" && this.msg != "error2"){
              this.myBool = true;  
              sessionStorage.setItem("bildeType",beskjed['bildeType']); 
              sessionStorage.setItem("userId",beskjed['userid']);
              sessionStorage.setItem("name",beskjed['email']); 
              sessionStorage.setItem("usertype",beskjed['usertype']); 
              location.reload();  
               return false;
            }else if(this.msg != "error2"){
              this.msg ="Vennligst fyll inn begge feltene"
            }else{
              this.msg = "Feil epost eller passord"
            }
            
        }) 
      
        

    }
    eksempel(){
      html`${this.eksempelFunksjon()}`
    } 
    eksempelFunksjon(){
        
    }

    deleteUser(e) {

        fetch(`${window.MyAppGlobals.serverURL}deleteUser`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },

        });
    }


}


customElements.define('inn-logging', TestELement);
